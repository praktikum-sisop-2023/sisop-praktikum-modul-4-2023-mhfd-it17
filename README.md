# sisop-praktikum-modul-4-2023-MHFD-IT17

## Nama Kelompok
1. Salmaa Satifha Dewi Rifma Putri  (5027211011)
2. Yoga Hartono                     (5027211023)
3. Dzakirozaan Uzlahwasata          (5027211066)

# Soal 1
## Analisis Soal
1. Men-download dataset pada Kaggle menggunakan command "kaggle datasets download -d bryanb/fifa-player-stats-database".
2. File yang berhasil didownload di-extract di dalam file storage.c.
3. Peb meminta bantuan untuk mencari pemain berpotensi tinggi yang bisa direkrut untuk klubnya, Manchester Blue.
4. Dataset yang digunakan adalah FIFA23_official_data.csv.
5. Kriteria pemain yang dicari adalah usia di bawah 25 tahun, potensi di atas 85, dan tidak bermain di klub Manchester City.
6. Informasi yang perlu dicetak meliputi nama pemain, klub tempat mereka bermain, umur, potensi, URL foto mereka, dan data lainnya.
7. Pekerjaan ini dilakukan di dalam file storage.c.

## Cara Pengerjaan
- Pertama-tama, dalam file storage.c, dilakukan download dataset dari Kaggle menggunakan command "kaggle datasets download -d bryanb/fifa-player-stats-database".
```
int main() {

   if (system("kaggle datasets download -d bryanb/fifa-player-stats-database") != 0)
    {
       printf("\e[31m\nDataset gagal didownload.\e[0m\n");
       return 1;
    }
```
- Selanjutnya, dataset yang telah di-download diekstrak menggunakan command "unzip fifa-player-stats-database.zip".
```

   if (system("unzip fifa-player-stats-database.zip") != 0)
    {
       printf("\e[31m\nDataset gagal diekstrak.\e[0m\n");
       return 1;
    }
       printf("\e[32m\nDataset berhasil didownload dan diekstrak.\e[0m\n");
```
- Setelah berhasil didownload dan diekstrak, dilakukan pembacaan file CSV khusus yang berisi informasi pemain sepak bola di seluruh dunia, termasuk Manchester Blue. File tersebut diberi nama FIFA23_official_data.csv.
```
    FILE *file_ptr;
    char line[MAX_LINE_LENGTH];
    Player players_array[MAX_PLAYERS];
    int num_players = 0;

    file_ptr = fopen("FIFA23_official_data.csv", "r");
    
    if (file_ptr == NULL) {
        printf("\e[31m\nGagal membuka file.\e[0m\n");
        return 1;
    }
```
- Dalam membaca file tersebut, digunakan fungsi fopen untuk membuka file dengan mode "r" (read).
```
file_ptr = fopen("FIFA23_official_data.csv", "r");
```
- Kemudian, dibuat array of struct yang berisi informasi pemain dengan tipe data Player. Array ini diberi nama players_array dan memiliki ukuran maksimal MAX_PLAYERS sebanyak 20000.
```
#define MAX_PLAYERS 20000
#define MAX_LINE_LENGTH 20000

typedef struct {
  char name[100];
  int age;
  int potential;
  char club[50];
  char nationality[100];
  char photo[100];
} Player;
```
- Selanjutnya, dilakukan penghitungan jumlah pemain yang memenuhi kriteria yang diminta oleh Peb yaitu usia di bawah 25 tahun, potensi di atas 85, dan tidak bermain di klub Manchester City. Pemain-pemain yang memenuhi kriteria tersebut akan disimpan dalam array players_array.
```
if (field_count > 0 && player_info.age < 25 && player_info.potential > 85 && strcmp(player_info.club, "Manchester City") != 0) {
            if (num_players >= MAX_PLAYERS) {
                printf("\e[31m\nJumlah player sudah mencapai maksimal.\e[0m\n");
                break;
            }
            players_array[num_players++] = player_info;
        }
    }

    fclose(file_ptr);
```
- Untuk melakukan hal tersebut, digunakan loop while untuk membaca baris demi baris dari file yang telah dibuka. Baris tersebut kemudian diproses dengan fungsi strtok untuk memisahkan kolom-kolom dalam satu baris.
```
fgets(line, sizeof(line), file_ptr); 

    while (fgets(line, sizeof(line), file_ptr)) {
        char *token = strtok(line, ",");
        int field_count = 0;
        Player player_info;

        while (token != NULL && field_count <= 8) {
            switch (field_count) {
                case 1:
                    strncpy(player_info.name, token, sizeof(player_info.name));
                    break;
                case 2:
                    player_info.age = atoi(token);
                    break;
                case 7:
                    player_info.potential = atoi(token);
                    break;
                case 8:
                    strncpy(player_info.club, token, sizeof(player_info.club));
                    break;
                case 4:
                    strncpy(player_info.nationality, token, sizeof(player_info.nationality));
                    break;
                case 3:
                    strncpy(player_info.photo, token, sizeof(player_info.photo));
                    break;
            }

            token = strtok(NULL, ",");
            field_count++;
        }
```
- Kolom-kolom tersebut kemudian disimpan ke dalam variabel-variabel pada tipe data Player sesuai dengan posisi kolomnya.
```
case 1:
                    strncpy(player_info.name, token, sizeof(player_info.name));
                    break;
                case 2:
                    player_info.age = atoi(token);
                    break;
                case 7:
                    player_info.potential = atoi(token);
                    break;
                case 8:
                    strncpy(player_info.club, token, sizeof(player_info.club));
                    break;
                case 4:
                    strncpy(player_info.nationality, token, sizeof(player_info.nationality));
                    break;
                case 3:
                    strncpy(player_info.photo, token, sizeof(player_info.photo));
                    break;
            }
```
- Jika pemain tersebut memenuhi kriteria yang diminta oleh Peb, informasi mengenai pemain tersebut akan disimpan dalam array players_array dengan menggunakan index num_players.
```
 if (field_count > 0 && player_info.age < 25 && player_info.potential > 85 && strcmp(player_info.club, "Manchester City") != 0) {
            if (num_players >= MAX_PLAYERS) {
                printf("\e[31m\nJumlah player sudah mencapai maksimal.\e[0m\n");
                break;
            }
            players_array[num_players++] = player_info;
```
- Jika jumlah pemain yang telah memenuhi kriteria sudah mencapai MAX_PLAYERS, maka program akan berhenti membaca file dan mencetak pesan error.
```
 if (num_players >= MAX_PLAYERS) {
                printf("\e[31m\nJumlah player sudah mencapai maksimal.\e[0m\n");
                break;
```
- Setelah selesai membaca file, program akan menutup file CSV yang telah dibuka menggunakan fungsi fclose.
```
  fclose(file_ptr);
```
- Terakhir, data-data pemain yang telah memenuhi kriteria akan dicetak ke layar menggunakan loop for. Cetakannya meliputi nama pemain, usia, potensi, klub tempat mereka bermain, URL foto mereka, dan data lainnya.
```
for (int i = 0; i < num_players; i++) {
        printf("\e[36mName:\e[0m %s\n", players_array[i].name);
        printf("\e[36mAge:\e[0m %d\n", players_array[i].age);
        printf("\e[36mPotential:\e[0m %d\n", players_array[i].potential);
        printf("\e[36mClub:\e[0m %s\n", players_array[i].club);
        printf("\e[36mNationality:\e[0m %s\n", players_array[i].nationality);
        printf("\e[36mPhoto:\e[0m %s\n", players_array[i].photo);
        printf("\e[33m--------------------\e[33m\n");
    }

    return 0;
}
```
## Source Code
```
#include <unistd.h>
#include <fcntl.h>
#include <dirent.h>
#include <errno.h>
#include <sys/time.h>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>

#define MAX_PLAYERS 20000
#define MAX_LINE_LENGTH 20000

typedef struct {
  char name[100];
  int age;
  int potential;
  char club[50];
  char nationality[100];
  char photo[100];
} Player;

int main() {

   if (system("kaggle datasets download -d bryanb/fifa-player-stats-database") != 0)
    {
       printf("\e[31m\nDataset gagal didownload.\e[0m\n");
       return 1;
    }

   if (system("unzip fifa-player-stats-database.zip") != 0)
    {
       printf("\e[31m\nDataset gagal diekstrak.\e[0m\n");
       return 1;
    }
       printf("\e[32m\nDataset berhasil didownload dan diekstrak.\e[0m\n");


    FILE *file_ptr;
    char line[MAX_LINE_LENGTH];
    Player players_array[MAX_PLAYERS];
    int num_players = 0;

    file_ptr = fopen("FIFA23_official_data.csv", "r");
    
    if (file_ptr == NULL) {
        printf("\e[31m\nGagal membuka file.\e[0m\n");
        return 1;
    }

    fgets(line, sizeof(line), file_ptr); 

    while (fgets(line, sizeof(line), file_ptr)) {
        char *token = strtok(line, ",");
        int field_count = 0;
        Player player_info;

        while (token != NULL && field_count <= 8) {
            switch (field_count) {
                case 1:
                    strncpy(player_info.name, token, sizeof(player_info.name));
                    break;
                case 2:
                    player_info.age = atoi(token);
                    break;
                case 7:
                    player_info.potential = atoi(token);
                    break;
                case 8:
                    strncpy(player_info.club, token, sizeof(player_info.club));
                    break;
                case 4:
                    strncpy(player_info.nationality, token, sizeof(player_info.nationality));
                    break;
                case 3:
                    strncpy(player_info.photo, token, sizeof(player_info.photo));
                    break;
            }

            token = strtok(NULL, ",");
            field_count++;
        }

        if (field_count > 0 && player_info.age < 25 && player_info.potential > 85 && strcmp(player_info.club, "Manchester City") != 0) {
            if (num_players >= MAX_PLAYERS) {
                printf("\e[31m\nJumlah player sudah mencapai maksimal.\e[0m\n");
                break;
            }
            players_array[num_players++] = player_info;
        }
    }

    fclose(file_ptr);

    for (int i = 0; i < num_players; i++) {
        printf("\e[36mName:\e[0m %s\n", players_array[i].name);
        printf("\e[36mAge:\e[0m %d\n", players_array[i].age);
        printf("\e[36mPotential:\e[0m %d\n", players_array[i].potential);
        printf("\e[36mClub:\e[0m %s\n", players_array[i].club);
        printf("\e[36mNationality:\e[0m %s\n", players_array[i].nationality);
        printf("\e[36mPhoto:\e[0m %s\n", players_array[i].photo);
        printf("\e[33m--------------------\e[33m\n");
    }

    return 0;
}

```

## Test Output
1. Mendownload dan mengekstrak data.csv 
![image](/uploads/d5ad619f235b0bea079c11dfc6f318ec/image.png)
2. Data yang sudah didownload dan diekstrak, serta sesuai kriteria soal
![image](/uploads/07080bf39f29c3c9ee57abd6b3d015e7/image.png)


# Soal 2
## Analisis Soal
Pada soal diberikan sebuah ketentuan dalam manajemen folder [.zip](https://drive.google.com/uc?export=download&id=1iUZuYjgwcnaaEPzREPgfU6L8TYJ9633i) yaitu : 
1. Apabila terdapat file yang mengandung kata restricted, maka file tidak dapat di-rename atau dihapus.
2. Apabila terdapat folder yang mengandung kata restricted, maka folder tersebut, folder yang ada di dalamnya, dan file yang ada di dalamnya tidak dapat di-rename ataupun dihapus.

Dari ketentuan ini diminta untuk :
1. Membuat folder productMagang pada folder /src_data/germa/products/restricted_list/. Kemudian, buatlah folder projectMagang pada /src_data/germa/projects/restricted_list/. Akan tetapi, hal tersebut akan gagal sesuai ketentuan yang diberikan.
2. Mengubah nama folder dari restricted_list pada folder /src_data/germa/projects/restricted_list/ menjadi /src_data/germa/projects/bypass_list/. Kemudian, membuat folder projectMagang di dalam folder bypass_list.
3. Mengubah folder filePenting di dalam folder projects menjadi restrictedFilePenting agar secure kembali karena folder sudah dapat diakses. Lalu mencoba mengubah nama file didalamnya.
4. Menghapus semua fileLama di dalam folder restrictedFileLama. Jangan lupa untuk menambahkan kata sihir pada folder tersebut agar folder tersebut dapat terhapus
5. Semua kegiatan yang dilakukan akan tercatat dalam sebuah file logmucatatsini.txt di luar file [.zip](https://drive.google.com/uc?export=download&id=1iUZuYjgwcnaaEPzREPgfU6L8TYJ9633i) yang diberikan.

## Cara Pengerjaan
- Membuat folder demo dengan command
```
mkdir demo
```
- Membuat file germa.c
```
nano germa.c
```
- Membuat fungsi untuk mendapatkan nama pengguna yang sedang menjalankan program
```
void get_user(char *username) {
    //mendapatkan UID dan informasi dari pengguna yang menjalankan program
    uid_t uid = geteuid(); 
    struct passwd *paswd = getpaswduid(uid);

    //jika berhasil
    if (paswd != NULL) {
        //nama pengguna diambil dan disalin ke username
        snprintf(username, 255, "%s", paswd->paswd_name);
    } else {
        //jika tidak akan dicetak error
        fprintf(stderr, "Gagal mendapat username.\n");
        exit(1);
    }
}
```
- Fungsi diatas mendapat user ID dengan getuid(), lalu akan didapatkan informasi lengkap dengan getpaswduid(uid) dan disimpan ke struct passwd. Lalu dilakukan pemeriksaan pengambilan informasi pengguna berhasil atau tidak. Jika tidak NULL berarti berhasil dan nama pengguna diambil untuk disimpan ke variabel username. Jika tidak berhasil akan tampil pesan error.

- Membuat fungsi untuk mendapat tanggal dan waktu real time dengan waktu lokal
```
int status;
//tanggal dan waktu
void getCurrentDateTime(char *dateTime) {
    time_t t = time(NULL);
    struct tm *tm = localtime(&t);
    strftime(datetime, 20, "%Y-%m-%d %H:%M:%S", tm);
}
```
- Membuat fungsi untuk menyimpan perintah, status, pengguna, dan deskripsi ke log
```
void saveStatusKeFile(const char *status, const char *command, const char *deskripsi) {
    char datetime[50];
    char username[255];

    getCurrentDateTime(datetime);
    get_user(username);

    FILE *file_Log = fopen("/home/alma/demo/logmucatatsini.txt", "a");

    if (file_Log != NULL) {
        fprintf(file_Log, "[%s]::%s::%s::%s-%s\n", status, datetime, command, username, deskripsi);
        fclose(file_Log);
    } else {
        printf("Gagal membuka file log.\n");
        exit(1);
    }
}
```
- Variabel datetime dan username digunakan untuk menyimpan nilai datetime (tanggal dan waktu) dan nama pengguna yang diperoleh dari pemanggilan fungsi getCurrentDateTime(datetime) dan get_user(username).
- Membuka file log dengan fopen(). Jika berhasil dibuka akan disimpan dalam variabel file_log.
- Setelah dibuka maka informasi akan dimasukkan dalam file log.
- Membuat fungsi untuk mendownload file berdasarkan link tertentu
```
void download_file(char *link_url, char *save_direktori){
    pid_t pid = fork(); 

    if (pid == 0) {
        execl("/usr/bin/wget", "wget", "--quiet", "--no-check-certificate", link_url, "-O", save_direktori, NULL);
        exit(0);
    } else if (pid > 0) {
        int status;
        waitpid(pid, &status, 0);

        if (WIFEXITED(status) && WEXITSTATUS(status) == 0) {
            saveStatusKeFile("SUCCESS", "DOWNLOAD", "Download completed successfully.");
        } else {
            saveStatusKeFile("FAILED", "DOWNLOAD", "Download failed.");
        }
    } else {
        fprintf(stderr, "Gagal pada proses fork.\n");
        exit(1);
    }
}
```
- Melakukan fork dan tercipta child process. Di child akan menjalankan wget untuk mengunduh. Jika berhasil akan keluar dari child dan menunggu sampai proses selesai. Setelah itu akan keluar status yang akan ditulis ke log sesuai statusnya. Jika gagal akan ada pesan error
- Membuat fungsi untuk melakukan unzip file.zip
```
void unzip_file(char *zip_file, char *unzip_direktori) {
    pid_t pid;
    int status;

    //diperiksa apa zip ada disistem
    if (access(zip_file, F_OK) == -1) {
        fprintf(stderr, "File zip tidak ditemukan.\n");
        exit(1);
    }

    pid = fork();
    if (pid == 0) { 
        //output ga ditampilin
        freopen("/dev/null", "w", stdout); 
        //nge unzip -> nimpa file yg ada saat ekstraksi
        execlp("unzip", "unzip", "-o", zip_file, "-d",  unzip_direktori, NULL);
        exit(1); 
    } else if (pid > 0) {
        waitpid(pid, &status, 0);
        if (WIFEXITED(status) && WEXITSTATUS(status) == 0) {
            saveStatusKeFile("SUCCESS", "UNZIP", "Unzip completed successfully.");
        } else {
            saveStatusKeFile("FAILED", "UNZIP", "Unzip failed.");
        }
    } else { 
        perror("fork");
        exit(1);
    }
}
```
- Dilakukan pemeriksaan file zip apa ada di sistem. Jika tidak akan tampil pesan error. Jika ada akan melakukan proses fork untuk melakukan unzip dan meletakkan di direktori yang sudah ditentukan. Serta akan memasukkan status nya ke dalam log.
- Menentukan path direktori utama
```
static  const  char *pathDirektori = "/home/alma/demo/nanaxgerma/src_data";
```
- Membuat program fuse untuk mencari file yang dibutuhkan
```
static  int  custom_getattr(const char *path, struct stat *stbuf)
{
    int res;
    char path_files[1024];
    snprintf(path_files, sizeof(path_files), "%s%s", pathDirektori, path);
    res = lstat(path_files, stbuf);
    if (res == -1) 
        return -errno;
    return 0;
}
```
- Membuat program fuse untuk membaca isi file dan mengembalikan data yang dibaca
```
static int custom_read(const char *path, char *buf, size_t size, off_t offset, struct fuse_file_info *fi)
{
    char path_files[1024];
    //ngecek path itu root atau tidak
    if(strcmp(path,"/") == 0)
    {
        path=pathDirektori;
        sprintf(path_files, sizeof(path_files), "%s",path);
    }
    else sprintf(path_files, sizeof(path_files), "%s%s",pathDirektori,path);

    int res = 0;
    int fd = 0 ;
    (void) fi;
    fd = open(path_files, O_RDONLY);
    if (fd == -1) 
        return -errno;
    res = pread(fd, buf, size, offset);

    if (res == -1) 
        res = -errno;
    close(fd);
    return res;
}
```
- Membuat program fuse untuk membaca isi direktori
```
static int custom_readdir(const char *path, void *buf, fuse_fill_dir_t filler, off_t offset, struct fuse_file_info *fi)
{
    char path_files[1024];
    if(strcmp(path,"/") == 0)
    {
        path=pathDirektori;
        snprintf(path_files, sizeof(path_files), "%s", path);
    } else 
    {
        snprintf(path_files, sizeof(path_files), "%s%s", pathDirektori, path);
    }

    int res = 0;
    DIR *dp;
    struct dirent *de;
    (void) offset;
    (void) fi;

    dp = opendir(path_files);

    if (dp == NULL) 
        return -errno;
    struct stat stbuf;
    memset(&stbuf, 0, sizeof(stbuf));

    char filepath[1024];
    snprintf(filepath, sizeof(filepath), "%s/%s", path_files, de->d_name);

    if (lstat(filepath, &stbuf) == -1) {
        return -errno;
    }

    int res = 0;
    res = filler(buf, de->d_name, &stbuf, 0);

    if (res != 0) {
        break;
    }
    closedir(dp);
    return 0;
}

```
- Membuat program fuse untuk membuat file
```
static int custom_create(const char *path, mode_t mode, struct fuse_file_info *fi)
{
    char path_files[1024];
    char text[100];
    if (strcmp(path, "/") == 0)
    {
        path = pathDirektori;
        snprintf(path_files, sizeof(path_files), "%s", path);
    }
    else
    {
        snprintf(path_files, sizeof(path_files), "%s%s", pathDirektori, path);
    }

    snprintf(text, sizeof(text), "Create file %s", path);

    // Mengecek kata kunci bypass dan restricted
    if (strstr(path, "restricted") != NULL)
    {
        // Jika path ada kata "restricted", log tidak berhasil
        saveStatusKeFile("FAILED", "CREATE", "Failed to create file");
        return -1;
    }
    else if (strstr(path, "bypass") != NULL && strstr(path, "restricted") != NULL)
    {
        // Jika path ada kata "bypass" dan "restricted", file dibuat
        int fd = creat(path_files, mode);
        if (fd == -1)
        {
            // Jika terjadi error, log tidak berhasil dan tampil error
            saveStatusKeFile("FAILED", "CREATE", "Failed to create file");
            return -errno;
        }
        else
        {
            // Jika berhasil, log sukses
            saveStatusKeFile("SUCCESS", "CREATE", text);
            close(fd);
            return 0;
        }
    }
    else
    {
        // Jika tidak ada kata "bypass" dan "restricted", file dibuat
        int fd = creat(path_files, mode);
        if (fd == -1)
        {
            // Jika terjadi error, log tidak berhasil dan tampil error
            saveStatusKeFile("FAILED", "CREATE", "Failed to create file");
            return -errno;
        }
        else
        {
            // Jika berhasil, log sukses
            saveStatusKeFile("SUCCESS", "CREATE", text);
            close(fd);
            return 0;
        }
    }
}
```
- Membuat program fuse untuk membuat folder
```
static int custom_mkdir(const char *path, mode_t mode)
{
    char path_files[1024];
    char text[100];
    if (strcmp(path, "/") == 0)
    {
        path = pathDirektori;
        snprintf(path_files, sizeof(path_files), "%s", path);
    }
    else
    {
        snprintf(path_files, sizeof(path_files), "%s%s", pathDirektori, path);
    }

    snprintf(text, sizeof(text), "Create directory %s", path);
    
    if (strstr(path, "restricted") != NULL)
    {
        // Jika path ada kata "restricted", log tidak berhasil
        saveStatusKeFile("FAILED", "MKDIR", text);
        return -1;
    }
    // Mengecek kata kunci bypass
    else if (strstr(path, "bypass") != NULL)
    {
        // Jika path ada kata "bypass", direktori dibuat
        int res = mkdir(path_files, mode);
        if (res == -1)
        {
            // Jika terjadi error, log tidak berhasil dan tampil error
            saveStatusKeFile("FAILED", "MKDIR", "Creating directory failed");
            return -errno;
        }
        else
        {
            // Jika berhasil, log sukses
            saveStatusKeFile("SUCCESS", "MKDIR", text);
            return res;
        }
    }
    else
    {
        // Jika tidak ada kata "bypass" dan "restricted", direktori akan dibuat
        int res = mkdir(path_files, mode);
        if (res == -1)
        {
            // Jika terjadi error, log tidak berhasil dan tampil error
            saveStatusKeFile("FAILED", "MKDIR", "Creating directory failed");
            return -errno;
        }
        else
        {
            // Jika berhasil, log sukses
            saveStatusKeFile("SUCCESS", "MKDIR", text);
            return res;
        }
    }
}
```
- Membuat program fuse untuk mengubah nama folder atau file
```
static int custom_rename(const char *asal, const char *tujuan)
{
    char fpathAsal[1024];
    char fpathTujuan[1024];
    char text[1024];
    
    if (strcmp(asal, "/") == 0)
    {
        asal = pathDirektori;
        sprintf(fpathAsal, "%s", asal);
    }
    else
    {
        sprintf(fpathAsal, "%s%s", pathDirektori, asal);
    }

    if (strcmp(tujuan, "/") == 0)
    {
        tujuan = pathDirektori;
        sprintf(fpathTujuan, "%s", tujuan);
    }
    else
    {
        sprintf(fpathTujuan, "%s%s", pathDirektori, tujuan);
    }

    sprintf(text, "Rename from %s to %s" , asal, tujuan);

    if (strstr(asal, "restricted") != NULL)
    {
        // Jika path ada kata hanya "restricted", log tdak berhasil
        saveStatusKeFile("FAILED", "RENAME", "Failed to rename file");
        return -1;
    }
    else if (strstr(asal, "restricted") != NULL && strstr(asal, "bypass") != NULL)
    {
        int res = rename(fpathAsal, fpathTujuan);;
        if (res == -1)
        {
            // Jika terjadi error, log tidak berhasil dan tampil error
            saveStatusKeFile("FAILED", "RENAME", "Failed to rename file");
            return -errno;
        }
        else
        {
            // Jika berhasil, log sukses
            saveStatusKeFile("SUCCESS", "RENAME", text);
            return 0;
        }
    }
    else
    {
        // Jika tidak ada kata "restricted" dan "bypass", file direname
        int res = rename(fpathAsal, fpathTujuan);
        if (res == -1)
        {
            // Jika terjadi error, log tidak berhasil dan tampil error
            saveStatusKeFile("FAILED", "RENAME", "Failed to rename file");
            return -errno;
        }
        else
        {
            // Jika berhasil, log sukses
            saveStatusKeFile("SUCCESS", "RENAME", text);
            return 0;
        }
    }
}
```
- Membuat program fuse untuk menghapus file
```
//Menghapus file
static int custom_unlink(const char *path)
{
    char path_files[1024];
    char text[100];
    if (strcmp(path, "/") == 0)
    {
        path = pathDirektori;
        sprintf(path_files, "%s", path);
    }
    else
    {
        sprintf(path_files, "%s%s", pathDirektori, path);
    }

    sprintf(text, "Remove file %s", path);

    if (strstr(path, "restricted") != NULL)
    {
        // Jika path ada kata hanya "restricted", log tidak berhasil
        saveStatusKeFile("FAILED", "RMFILE", "Failed to remove file");
        return -1;
    }
    //Mengecek kata "restricted" dan "bypass" di path
    else if (strstr(path, "restricted") != NULL && strstr(path, "bypass") != NULL)
    {
        int res = unlink(path_files);
        if (res == -1)
        {
            // Jika terjadi error, log tidak berhasil dan tampil error
            saveStatusKeFile("FAILED", "RMFILE", "Failed to remove file");
            return -errno;
        }
        else
        {
            // Jika berhasil, log sukses
            saveStatusKeFile("SUCCESS", "RMFILE", text);
            return 0;
        }
    }
    else
    {
        // Jika tidak ada kata "restricted" dan "bypass", file dihapus
        int res = unlink(path_files);
        if (res == -1)
        {
            // Jika terjadi error, log tidak berhasil dan tampil error
            saveStatusKeFile("FAILED", "RMFILE", "Failed to remove file");
            return -errno;
        }
        else
        {
            // Jika berhasil, log sukses
            saveStatusKeFile("SUCCESS", "RMFILE", text);
            return 0;
        }
    }
}
```
- Membuat program fuse untuk menghapus folder
```
static int custom_rmdir(const char *path)
{
    char path_files[1024];
    char text[100];
    if (strcmp(path, "/") == 0)
    {
        path = pathDirektori;
        sprintf(path_files, "%s", path);
    }
    else
    {
        sprintf(path_files, "%s%s", pathDirektori, path);
    }

    sprintf(text, "Remove directory %s", path);

    // Mengecek kata kunci restricted
    if (strstr(path, "restricted") != NULL)
    {
        // Jika path ada kata "restricted", log tidak berhasil
        saveStatusKeFile("FAIL", "REMOVE", text);
        return -1;
    }
    // Mengecek kata kunci bypass
    else if (strstr(path, "bypass") != NULL)
    {
        // Jika path ada kata "bypass", direktori dihapus
        int res = rmdir(path_files);
        if (res == -1)
        {
            // Jika terjadi error, log tidak berhasil dan tampil error
            saveStatusKeFile("FAIL", "REMOVE", text);
            return -errno;
        }
        else
        {
            // Jika berhasil, log sukses
            saveStatusKeFile("SUCCESS", "REMOVE", text);
            return 0;
        }
    }
    else
    {
        // Jika tidak ada kata "bypass", maka log tidak berhasil
        saveStatusKeFile("FAIL", "REMOVE", text);
        return -1;
    }
}
```
- Membuat fungsi operator fuse
```
static struct fuse_operations xmp_oper = {
    .getattr = custom_getattr,
    .readdir = custom_readdir, 
    .read = custom_read,
    .mkdir = custom_mkdir,
    .create = custom_create, 
    .rename = custom_rename, 
    .unlink = custom_unlink, 
    .rmdir = custom_rmdir,  
};
```
- Membuat inisialisasi untuk menjalankan program download, unzip dari link tertentu dan menempatkan di direktori yang ditentukan
```
void init(){
    download_file(url, "./nanaxgerma.zip");
    char *url = "https://drive.google.com/uc?export=download&id=1iUZuYjgwcnaaEPzREPgfU6L8TYJ9633i";
   
    char* unzip_direktori = "/home/alma/demo";
    char* zip_file = "nanaxgerma.zip";
    unzip_file(zip_file, unzip_direktori);
}
```
- Membuat entry point utama dari program untuk menjalankan seluruh perintah operator fuse
```
int main(int argc, char *argv[]) {
    const char *status = "SUCCESS";
    const char *command = "SOME_COMMAND";
    const char *deskripsi = "desk";

    saveStatusKeFile(status, command, deskripsi);

    pid_t pid;
    pid = fork(); 
    //Parent process akan melakukan exit dengan status success (EXIT_SUCCESS), sedangkan child process 
    //akan melanjutkan untuk menjalankan fungsi init().
    if (pid<0){
        exit(EXIT_FAILURE);
    }
    if (pid > 0){
        exit(EXIT_SUCCESS);
    }

    init();
    umask(0);
    //untuk menjalankan FUSE dengan menggunakan operasi yang didefinisikan
    return fuse_main(argc, argv, &xmp_oper, NULL);
    return 0;
}
```

## Source Code
```
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <dirent.h>
#include <sys/stat.h>
#include <sys/types.h>
#include <unistd.h>
#include <wait.h>
#include <fuse.h>
#include <time.h>
#include <paswdd.h>
#include <fcntl.h>
#include <errno.h>

#define FUSE_USE_VERSION 28
#define HOME "/home/alma/demo/"

//username dari UID yang menjalankan program.
void get_user(char *username) {
    //mendapatkan UID dan informasi dari pengguna yang menjalankan program
    uid_t uid = geteuid(); 
    struct passwd *paswd = getpaswduid(uid);

    //jika berhasil
    if (paswd != NULL) {
        //nama pengguna diambil dan disalin ke username
        snprintf(username, 255, "%s", paswd->paswd_name);
    } else {
        //jika tidak akan dicetak error
        fprintf(stderr, "Gagal mendapat username.\n");
        exit(1);
    }
}

int status;
//tanggal dan waktu
void getCurrentDateTime(char *dateTime) {
    time_t t = time(NULL);
    struct tm *tm = localtime(&t);
    strftime(datetime, 20, "%Y-%m-%d %H:%M:%S", tm);
}

void saveStatusKeFile(const char *status, const char *command, const char *deskripsi) {
    char datetime[50];
    char username[255];

    getCurrentDateTime(datetime);
    get_user(username);

    FILE *file_Log = fopen("/home/alma/demo/logmucatatsini.txt", "a");

    if (file_Log != NULL) {
        fprintf(file_Log, "[%s]::%s::%s::%s-%s\n", status, datetime, command, username, deskripsi);
        fclose(file_Log);
    } else {
        printf("Gagal membuka file log.\n");
        exit(1);
    }
}

//Download file dari drive yang ditentukan
void download_file(char *link_url, char *save_direktori){
    pid_t pid = fork(); 

    if (pid == 0) {
        execl("/usr/bin/wget", "wget", "--quiet", "--no-check-certificate", link_url, "-O", save_direktori, NULL);
        exit(0);
    } else if (pid > 0) {
        int status;
        waitpid(pid, &status, 0);

        if (WIFEXITED(status) && WEXITSTATUS(status) == 0) {
            saveStatusKeFile("SUCCESS", "DOWNLOAD", "Download completed successfully.");
        } else {
            saveStatusKeFile("FAILED", "DOWNLOAD", "Download failed.");
        }
    } else {
        fprintf(stderr, "Gagal pada proses fork.\n");
        exit(1);
    }
}

//unzip foldernya
void unzip_file(char *zip_file, char *unzip_direktori) {
    pid_t pid;
    int status;

    //diperiksa apa zip ada disistem
    if (access(zip_file, F_OK) == -1) {
        fprintf(stderr, "File zip tidak ditemukan.\n");
        exit(1);
    }

    pid = fork();
    if (pid == 0) { 
        //output ga ditampilin
        freopen("/dev/null", "w", stdout); 
        //nge unzip -> nimpa file yg ada saat ekstraksi
        execlp("unzip", "unzip", "-o", zip_file, "-d",  unzip_direktori, NULL);
        exit(1); 
    } else if (pid > 0) {
        waitpid(pid, &status, 0);
        if (WIFEXITED(status) && WEXITSTATUS(status) == 0) {
            saveStatusKeFile("SUCCESS", "UNZIP", "Unzip completed successfully.");
        } else {
            saveStatusKeFile("FAILED", "UNZIP", "Unzip failed.");
        }
    } else { 
        perror("fork");
        exit(1);
    }
}

static  const  char *pathDirektori = "/home/alma/demo/nanaxgerma/src_data";

//mencari file, mengelola file, dan simpan data
//mencari file yang dibutuhkan
static  int  custom_getattr(const char *path, struct stat *stbuf)
{
    int res;
    char path_files[1024];
    snprintf(path_files, sizeof(path_files), "%s%s", pathDirektori, path);
    res = lstat(path_files, stbuf);
    if (res == -1) 
        return -errno;
    return 0;
}

//membaca isi file dan mengembalikan data yang dibaca
static int custom_read(const char *path, char *buf, size_t size, off_t offset, struct fuse_file_info *fi)
{
    char path_files[1024];
    //ngecek path itu root atau tidak
    if(strcmp(path,"/") == 0)
    {
        path=pathDirektori;
        sprintf(path_files, sizeof(path_files), "%s",path);
    }
    else sprintf(path_files, sizeof(path_files), "%s%s",pathDirektori,path);

    int res = 0;
    int fd = 0 ;
    (void) fi;
    fd = open(path_files, O_RDONLY);
    if (fd == -1) 
        return -errno;
    res = pread(fd, buf, size, offset);

    if (res == -1) 
        res = -errno;
    close(fd);
    return res;
}

//membaca isi direktori
static int custom_readdir(const char *path, void *buf, fuse_fill_dir_t filler, off_t offset, struct fuse_file_info *fi)
{
    char path_files[1024];
    if(strcmp(path,"/") == 0)
    {
        path=pathDirektori;
        snprintf(path_files, sizeof(path_files), "%s", path);
    } else 
    {
        snprintf(path_files, sizeof(path_files), "%s%s", pathDirektori, path);
    }

    int res = 0;
    DIR *dp;
    struct dirent *de;
    (void) offset;
    (void) fi;

    dp = opendir(path_files);

    if (dp == NULL) 
        return -errno;
    struct stat stbuf;
    memset(&stbuf, 0, sizeof(stbuf));

    char filepath[1024];
    snprintf(filepath, sizeof(filepath), "%s/%s", path_files, de->d_name);

    if (lstat(filepath, &stbuf) == -1) {
        return -errno;
    }

    int res = 0;
    res = filler(buf, de->d_name, &stbuf, 0);

    if (res != 0) {
        break;
    }
    closedir(dp);
    return 0;
}

static int custom_create(const char *path, mode_t mode, struct fuse_file_info *fi)
{
    char path_files[1024];
    char text[100];
    if (strcmp(path, "/") == 0)
    {
        path = pathDirektori;
        snprintf(path_files, sizeof(path_files), "%s", path);
    }
    else
    {
        snprintf(path_files, sizeof(path_files), "%s%s", pathDirektori, path);
    }

    snprintf(text, sizeof(text), "Create file %s", path);

    // Mengecek kata kunci bypass dan restricted
    if (strstr(path, "restricted") != NULL)
    {
        // Jika path ada kata "restricted", log tidak berhasil
        saveStatusKeFile("FAILED", "CREATE", "Failed to create file");
        return -1;
    }
    else if (strstr(path, "bypass") != NULL && strstr(path, "restricted") != NULL)
    {
        // Jika path ada kata "bypass" dan "restricted", file dibuat
        int fd = creat(path_files, mode);
        if (fd == -1)
        {
            // Jika terjadi error, log tidak berhasil dan tampil error
            saveStatusKeFile("FAILED", "CREATE", "Failed to create file");
            return -errno;
        }
        else
        {
            // Jika berhasil, log sukses
            saveStatusKeFile("SUCCESS", "CREATE", text);
            close(fd);
            return 0;
        }
    }
    else
    {
        // Jika tidak ada kata "bypass" dan "restricted", file dibuat
        int fd = creat(path_files, mode);
        if (fd == -1)
        {
            // Jika terjadi error, log tidak berhasil dan tampil error
            saveStatusKeFile("FAILED", "CREATE", "Failed to create file");
            return -errno;
        }
        else
        {
            // Jika berhasil, log sukses
            saveStatusKeFile("SUCCESS", "CREATE", text);
            close(fd);
            return 0;
        }
    }
}

//membuat folder
static int custom_mkdir(const char *path, mode_t mode)
{
    char path_files[1024];
    char text[100];
    if (strcmp(path, "/") == 0)
    {
        path = pathDirektori;
        snprintf(path_files, sizeof(path_files), "%s", path);
    }
    else
    {
        snprintf(path_files, sizeof(path_files), "%s%s", pathDirektori, path);
    }

    snprintf(text, sizeof(text), "Create directory %s", path);
    
    if (strstr(path, "restricted") != NULL)
    {
        // Jika path ada kata "restricted", log tidak berhasil
        saveStatusKeFile("FAILED", "MKDIR", text);
        return -1;
    }
    // Mengecek kata kunci bypass
    else if (strstr(path, "bypass") != NULL)
    {
        // Jika path ada kata "bypass", direktori dibuat
        int res = mkdir(path_files, mode);
        if (res == -1)
        {
            // Jika terjadi error, log tidak berhasil dan tampil error
            saveStatusKeFile("FAILED", "MKDIR", "Creating directory failed");
            return -errno;
        }
        else
        {
            // Jika berhasil, log sukses
            saveStatusKeFile("SUCCESS", "MKDIR", text);
            return res;
        }
    }
    else
    {
        // Jika tidak ada kata "bypass" dan "restricted", direktori akan dibuat
        int res = mkdir(path_files, mode);
        if (res == -1)
        {
            // Jika terjadi error, log tidak berhasil dan tampil error
            saveStatusKeFile("FAILED", "MKDIR", "Creating directory failed");
            return -errno;
        }
        else
        {
            // Jika berhasil, log sukses
            saveStatusKeFile("SUCCESS", "MKDIR", text);
            return res;
        }
    }
}

static int custom_rename(const char *asal, const char *tujuan)
{
    char fpathAsal[1024];
    char fpathTujuan[1024];
    char text[1024];
    
    if (strcmp(asal, "/") == 0)
    {
        asal = pathDirektori;
        sprintf(fpathAsal, "%s", asal);
    }
    else
    {
        sprintf(fpathAsal, "%s%s", pathDirektori, asal);
    }

    if (strcmp(tujuan, "/") == 0)
    {
        tujuan = pathDirektori;
        sprintf(fpathTujuan, "%s", tujuan);
    }
    else
    {
        sprintf(fpathTujuan, "%s%s", pathDirektori, tujuan);
    }

    sprintf(text, "Rename from %s to %s" , asal, tujuan);

    if (strstr(asal, "restricted") != NULL)
    {
        // Jika path ada kata hanya "restricted", log tdak berhasil
        saveStatusKeFile("FAILED", "RENAME", "Failed to rename file");
        return -1;
    }
    else if (strstr(asal, "restricted") != NULL && strstr(asal, "bypass") != NULL)
    {
        int res = rename(fpathAsal, fpathTujuan);;
        if (res == -1)
        {
            // Jika terjadi error, log tidak berhasil dan tampil error
            saveStatusKeFile("FAILED", "RENAME", "Failed to rename file");
            return -errno;
        }
        else
        {
            // Jika berhasil, log sukses
            saveStatusKeFile("SUCCESS", "RENAME", text);
            return 0;
        }
    }
    else
    {
        // Jika tidak ada kata "restricted" dan "bypass", file direname
        int res = rename(fpathAsal, fpathTujuan);
        if (res == -1)
        {
            // Jika terjadi error, log tidak berhasil dan tampil error
            saveStatusKeFile("FAILED", "RENAME", "Failed to rename file");
            return -errno;
        }
        else
        {
            // Jika berhasil, log sukses
            saveStatusKeFile("SUCCESS", "RENAME", text);
            return 0;
        }
    }
}

//Menghapus file
static int custom_unlink(const char *path)
{
    char path_files[1024];
    char text[100];
    if (strcmp(path, "/") == 0)
    {
        path = pathDirektori;
        sprintf(path_files, "%s", path);
    }
    else
    {
        sprintf(path_files, "%s%s", pathDirektori, path);
    }

    sprintf(text, "Remove file %s", path);

    if (strstr(path, "restricted") != NULL)
    {
        // Jika path ada kata hanya "restricted", log tidak berhasil
        saveStatusKeFile("FAILED", "RMFILE", "Failed to remove file");
        return -1;
    }
    //Mengecek kata "restricted" dan "bypass" di path
    else if (strstr(path, "restricted") != NULL && strstr(path, "bypass") != NULL)
    {
        int res = unlink(path_files);
        if (res == -1)
        {
            // Jika terjadi error, log tidak berhasil dan tampil error
            saveStatusKeFile("FAILED", "RMFILE", "Failed to remove file");
            return -errno;
        }
        else
        {
            // Jika berhasil, log sukses
            saveStatusKeFile("SUCCESS", "RMFILE", text);
            return 0;
        }
    }
    else
    {
        // Jika tidak ada kata "restricted" dan "bypass", file dihapus
        int res = unlink(path_files);
        if (res == -1)
        {
            // Jika terjadi error, log tidak berhasil dan tampil error
            saveStatusKeFile("FAILED", "RMFILE", "Failed to remove file");
            return -errno;
        }
        else
        {
            // Jika berhasil, log sukses
            saveStatusKeFile("SUCCESS", "RMFILE", text);
            return 0;
        }
    }
}

static int custom_rmdir(const char *path)
{
    char path_files[1024];
    char text[100];
    if (strcmp(path, "/") == 0)
    {
        path = pathDirektori;
        sprintf(path_files, "%s", path);
    }
    else
    {
        sprintf(path_files, "%s%s", pathDirektori, path);
    }

    sprintf(text, "Remove directory %s", path);

    // Mengecek kata kunci restricted
    if (strstr(path, "restricted") != NULL)
    {
        // Jika path ada kata "restricted", log tidak berhasil
        saveStatusKeFile("FAIL", "REMOVE", text);
        return -1;
    }
    // Mengecek kata kunci bypass
    else if (strstr(path, "bypass") != NULL)
    {
        // Jika path ada kata "bypass", direktori dihapus
        int res = rmdir(path_files);
        if (res == -1)
        {
            // Jika terjadi error, log tidak berhasil dan tampil error
            saveStatusKeFile("FAIL", "REMOVE", text);
            return -errno;
        }
        else
        {
            // Jika berhasil, log sukses
            saveStatusKeFile("SUCCESS", "REMOVE", text);
            return 0;
        }
    }
    else
    {
        // Jika tidak ada kata "bypass", maka log tidak berhasil
        saveStatusKeFile("FAIL", "REMOVE", text);
        return -1;
    }
}

//Membuat fungsi operator fuse
static struct fuse_operations xmp_oper = {
    .getattr = custom_getattr,
    .readdir = custom_readdir, 
    .read = custom_read,
    .mkdir = custom_mkdir,
    .create = custom_create, 
    .rename = custom_rename, 
    .unlink = custom_unlink, 
    .rmdir = custom_rmdir,  
};

void init(){
    download_file(url, "./nanaxgerma.zip");
    char *url = "https://drive.google.com/uc?export=download&id=1iUZuYjgwcnaaEPzREPgfU6L8TYJ9633i";
   
    char* unzip_direktori = "/home/alma/demo";
    char* zip_file = "nanaxgerma.zip";
    unzip_file(zip_file, unzip_direktori);
    }

int main(int argc, char *argv[]) {
    const char *status = "SUCCESS";
    const char *command = "SOME_COMMAND";
    const char *deskripsi = "desk";

    saveStatusKeFile(status, command, deskripsi);

    pid_t pid;
    pid = fork(); 
    //Parent process akan melakukan exit dengan status success (EXIT_SUCCESS), sedangkan child process 
    //akan melanjutkan untuk menjalankan fungsi init().
    if (pid<0){
        exit(EXIT_FAILURE);
    }
    if (pid > 0){
        exit(EXIT_SUCCESS);
    }

    init();
    umask(0);
    //untuk menjalankan FUSE dengan menggunakan operasi yang didefinisikan
    return fuse_main(argc, argv, &xmp_oper, NULL);
    return 0;
}
```
## Test Output
1. Membuat folder productMagang dan projectMagang tetapi gagal
![soal1](https://i.ibb.co/xzRNz9P/image.png)
2. Me-rename folder restricted_list menjadi bypass_list, lalu membuat folder projectMagang didalamnya
![soal1](https://i.ibb.co/ykPLKnf/image.png)
3. Me-rename folder filePenting di dalam folder projects menjadi restrictedFilePenting, lalu mencoba rename file didalamnya
![soal1](https://i.ibb.co/dPq56b6/image.png)
4. Menghapus semua fileLama di dalam folder restrictedFileLama
![soal1](https://i.ibb.co/18ZzXqL/image.png)
5. Hasil log pada logmucatatsini.txt
```
[SUCCESS]::03/06/2023-09:37:05::DOWNLOAD::alma-Download completed successfully.
[SUCCESS]::03/06/2023-09:37:05::UNZIP::alma-Unzip completed successfully.
[FAILED]::03/06/2023-09:43:41::MKDIR::alma-Create directory /germaa/products/restricted_list/productMagang
[FAILED]::03/06/2023-09:43:55::MKDIR::alma-Create directory /germaa/projects/restricted_list/projectMagang
[FAILED]::03/06/2023-09:45:33::RENAME::alma-Failed to rename file
[SUCCESS]::03/06/2023-09:46:13::RENAME::alma-Rename from /germaa/projects to /germaa/projectbypass
[SUCCESS]::03/06/2023-09:47:08::RENAME::alma-Rename from /germaa/projectbypass/restricted_list to /germaa/projectbypass/bypass_list
[SUCCESS]::03/06/2023-09:47:39::MKDIR::alma-Create directory /germaa/projectbypass/bypass_list/projectMagang
[SUCCESS]::03/06/2023-09:48:26::RENAME::alma-Rename from /germaa/projectbypass/bypass_list/filePenting to /germaa/projectbypass/bypass_list/restrictedFilePenting
[SUCCESS]::03/06/2023-09:50:38::RENAME::alma-Rename from /germaa/projectbypass/bypass_list/restrictedFilePenting/ourProject.txt to /germaa/projectbypass/bypass_list/restrictedFilePenting/ourProjectrename.txt
[FAILED]::03/06/2023-09:52:29::RMFILE::alma-Failed to remove file
[FAILED]::03/06/2023-09:52:29::RMFILE::alma-Failed to remove file
[SUCCESS]::03/06/2023-09:52:50::RENAME::alma-Rename from /others to /othersbypass
[SUCCESS]::03/06/2023-09:53:25::RMFILE::alma-Remove file /othersbypass/restrictedFileLama/fileLama1.txt
[SUCCESS]::03/06/2023-09:53:25::RMFILE::alma-Remove file /othersbypass/restrictedFileLama/fileLama2.txt
[SUCCESS]::03/06/2023-09:53:25::REMOVE::alma-Remove directory /othersbypass/restrictedFileLama

```
# Soal 2
## Analisis Soal

## Cara Pengerjaan

## Source Code
```sh

```
## Tes Output


# Soal 3
## Analisis Soal
- Membuat sebuah FUSE yang termodifikasi dengan source mount-nya adalah /etc yang bernama secretadmirer.c.
- Buat semua direktori dan file yang berada pada direktori yang diawali dengan huruf L, U, T, dan H akan ter-encode dengan Base64.
- Encode berlaku rekursif, artinya berlaku untuk direktori dan file yang baru di-rename atau pun yang baru dibuat.
- Untuk membedakan antara file dan direktori, Dhafin ingin semua format penamaan file menggunakan lowercase sedangkan untuk direktori semua harus uppercase.
- Untuk nama file atau direktori yang hurufnya kurang dari atau sama dengan empat, maka ubah namanya menggunakan data format binary yang didapat dari ASCII code masing-masing karakter.
- Setiap ingin membuka file, maka harus memasukkan password terlebih dahulu (set password tidak ditentukan).
- Buatlah mounting FUSE tersebut di dalam Docker Container menggunakan Docker Compose dengan base image Ubuntu Bionic.
- Terdapat dua buah container yang akan digunakan: Container bernama Dhafin akan melakukan mount FUSE yang telah dimodifikasi, dan container bernama Normal akan melakukan mount yang hanya menampilkan /etc yang normal tanpa ada modifikasi apapun.
- Terdapat sebuah image bernama fhinnn/sisop23 yang dapat dieksekusi untuk membantu melihat hasilnya.
Catatan:

FUSE tersebut harus mendukung operasi mkdir, cat, dan mv.

## Cara Pengerjaan
1. Impor library yang diperlukan:
- fuse.h
- stdio.h
- string.h
- unistd.h
- fcntl.h
- dirent.h
- errno.h
- sys/time.h
- stdlib.h
- stdint.h
- ctype.h
- stdbool.h
2. Tentukan variabel konstan `dirpath` yang berisi path direktori tujuan.
3. Buat array karakter `base64_table` yang berisi karakter-karakter untuk enkripsi base64.
4. Implementasikan fungsi `base64_encode` yang menerima input berupa string dan mengembalikan string hasil enkripsi base64. Fungsi ini akan mengalokasikan memori untuk menyimpan hasil enkripsi.
5. Implementasikan fungsi `toUpper` yang menerima input berupa string dan mengubah semua karakter menjadi huruf kapital. Fungsi ini akan mengembalikan string hasil perubahan.
6. Implementasikan fungsi `toLower` yang menerima input berupa string dan mengubah semua karakter menjadi huruf kecil. Fungsi ini akan mengembalikan string hasil perubahan.
7. Implementasikan fungsi `getBinaryRepresentation` yang menerima input berupa string dan mengembalikan representasi biner dari setiap karakter dalam string tersebut. Fungsi ini akan mengalokasikan memori untuk menyimpan hasil representasi biner.
8. Implementasikan fungsi `xmp_getattr` yang merupakan implementasi dari operasi getattr pada FUSE. Fungsi ini akan mengambil atribut file dari path yang diberikan dan mengisinya ke dalam struktur stat yang diberikan.
9. Implementasikan fungsi `xmp_readdir` yang merupakan implementasi dari operasi readdir pada FUSE. Fungsi ini akan membaca isi direktori yang diberikan dan mengisi struktur dirent dengan informasi setiap entri.
10. Implementasikan fungsi `xmp_read` yang merupakan implementasi dari operasi read pada FUSE. Fungsi ini akan membaca konten file yang diberikan dan mengisinya ke dalam buffer yang diberikan.
11. Implementasikan fungsi `xmp_mkdir` yang merupakan implementasi dari operasi mkdir pada FUSE. Fungsi ini akan membuat direktori baru dengan mode yang diberikan pada path yang diberikan.
12. Implementasikan fungsi `xmp_rename` yang merupakan implementasi dari operasi rename pada FUSE. Fungsi ini akan mengubah nama dari file/direktori yang diberikan menjadi nama baru yang diberikan.
13. Implementasikan fungsi `verifyPassword` yang menerima password yang dimasukkan dan memverifikasi apakah password tersebut benar.
14. Implementasikan fungsi `xmp_open` yang merupakan implementasi dari operasi open pada FUSE. Fungsi ini akan membuka file yang diberikan dengan menggunakan flag yang diberikan. Sebelum membuka file, fungsi ini akan meminta pengguna memasukkan password dan memverifikasinya.
15. Inisialisasi struktur `fuse_operations` dengan fungsi-fungsi yang telah diimplementasikan sebelumnya.
16. Di dalam fungsi main, panggil fungsi umask(0) untuk mengatur permission pada file dan direktori yang dibuat.
17. Panggil fungsi fuse_main dengan argumen argc dan argv, serta struktur fuse_operations yang telah diinisialisasi.
18. Kompilasi dan jalankan program untuk menggunakan sistem file yang telah diimplementasikan.

## Source Code
```
#define FUSE_USE_VERSION 28
#include <fuse.h>
#include <stdio.h>
#include <string.h>
#include <unistd.h>
#include <fcntl.h>
#include <dirent.h>
#include <errno.h>
#include <sys/time.h>
#include <stdlib.h>
#include <stdint.h>
#include <ctype.h>
#include <stdbool.h> 

static const char *dirpath = "/home/dzaki/sisop/inifolderetc/sisop";

static const char base64_table[] = "ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz0123456789+/";

char* base64_encode(const char* input) {
    size_t input_length = strlen(input);
    size_t output_length = 4 * ((input_length + 2) / 3); // Calculate the output length

    char* encoded_data = (char*)malloc(output_length + 1); // Allocate memory for the encoded data

    if (encoded_data == NULL) {
        printf("Memory allocation failed.\n");
        return NULL;
    }

    size_t i, j;
    for (i = 0, j = 0; i < input_length;) {
        uint32_t octet_a = i < input_length ? (unsigned char)input[i++] : 0;
        uint32_t octet_b = i < input_length ? (unsigned char)input[i++] : 0;
        uint32_t octet_c = i < input_length ? (unsigned char)input[i++] : 0;

        uint32_t triple = (octet_a << 16) + (octet_b << 8) + octet_c;

        encoded_data[j++] = base64_table[(triple >> 18) & 0x3F];
        encoded_data[j++] = base64_table[(triple >> 12) & 0x3F];
        encoded_data[j++] = base64_table[(triple >> 6) & 0x3F];
        encoded_data[j++] = base64_table[triple & 0x3F];
    }

    // Pad the encoded data if necessary
    size_t padding = input_length % 3;
    for (i = 0; i < padding; i++) {
        encoded_data[output_length - 1 - i] = '=';
    }

    encoded_data[output_length] = '\0'; // Add null terminator

    return encoded_data;
}

char* toUpper(char* str) {
    int i = 0;
    while (str[i]) {
        str[i] = toupper(str[i]);
        i++;
    }
    return str;
}

char* toLower(char* str) {
    int i = 0;
    while (str[i]) {
        str[i] = tolower(str[i]);
        i++;
    }
    return str;
}

char* getBinaryRepresentation(const char* str) {
    int length = strlen(str);
    char* binaryStr = (char*)malloc((8 * length + 1) * sizeof(char)); // Allocate memory for binary string

    if (binaryStr == NULL) {
        printf("Memory allocation failed.\n");
        return NULL;
    }

    int index = 0;
    for (int i = 0; i < length; i++) {
        unsigned char ch = (unsigned char)str[i]; // Cast to unsigned char to ensure consistent behavior
        for (int j = 7; j >= 0; j--) {
            binaryStr[index++] = ((ch >> j) & 1) + '0'; // Convert bit to character '0' or '1'
        }
        binaryStr[index++] = ' ';
    }
    binaryStr[index - 1] = '\0'; // Replace the last space with null character

    return binaryStr;
}

static int xmp_getattr(const char *path, struct stat *stbuf) {
    int res;
    char fpath[1000];

    sprintf(fpath, "%s%s", dirpath, path);

    res = lstat(fpath, stbuf);

    if (res == -1)
        return -errno;

    return 0;
}

static int xmp_readdir(const char *path, void *buf, fuse_fill_dir_t filler, off_t offset, struct fuse_file_info *fi) {
    char fpath[1000];

    if (strcmp(path, "/") == 0) {
        path = dirpath;
        sprintf(fpath, "%s", path);
    } else
        sprintf(fpath, "%s%s", dirpath, path);

    int res = 0;

    DIR *dp;
    struct dirent *de;
    (void)offset;
    (void)fi;

    dp = opendir(fpath);
    //printf("%s\n",fpath);
    if (dp == NULL)
        return -errno;

    while ((de = readdir(dp)) != NULL) {
    
        char f[1000], base[1000], opath[1000], npath[1000];
        strcpy(f,de->d_name);
        
        if(strlen(f) <= 4 )
        {
            strcpy(base,getBinaryRepresentation(f));
            //printf("bin - %s -> %s\n", f, base);
        }
        else if(de->d_type == DT_DIR && (f[0] != 0 && f[0] != 1))
        {
            strcpy(base,toUpper(f));
            //printf("dir - %s -> %s\n", f, base);
        }
        else if(de->d_type == DT_REG && (f[0] != 0 && f[0] != 1))
        {
            strcpy(base,toLower(f));
            //printf("file - %s -> %s\n", f, base);
        }
        else
            strcpy(base,f);
        
        strcpy(opath,fpath);
    	strcpy(npath,fpath);
    	strcat(opath,"/");
    	strcat(npath,"/");
    	strcat(opath,de->d_name);
    	strcat(npath,base);
    	
        
    	if ((de->d_type == DT_REG) && (
    	     f[0] == 'L' || f[0] == 'U' || f[0] == 'T' || f[0] == 'H' ||
             f[0] == 'l' || f[0] == 'u' || f[0] == 't' || f[0] == 'h'))
    	{
    	     // Open the file for reading
            FILE* file = fopen(opath, "rb");
            if (file != NULL) {
                // Determine the file size
                fseek(file, 0, SEEK_END);
                long file_size = ftell(file);
                fseek(file, 0, SEEK_SET);

                // Allocate memory for file content
                char* file_content = (char*)malloc(file_size);
                if (file_content != NULL) {
                    // Read the file content
                    fread(file_content, 1, file_size, file);
                    
                    if(strchr(file_content, '!') == NULL){
		             // Encode the file content with base64
		            char* encoded_content = base64_encode(file_content);
		            strcat(encoded_content,"\n!");
		            if (encoded_content != NULL) {
		                // Create a new file with the encoded content
		                FILE* encoded_file = fopen(npath, "wb");
		                if (encoded_file != NULL) {
		                    // Write the encoded content to the file
		                    fwrite(encoded_content, 1, strlen(encoded_content), encoded_file);
		                    fclose(encoded_file);
		                } else {
		                    printf("Failed to create the encoded file: %s\n", npath);
		                }
		                
		                free(encoded_content);
		            } else {
		                printf("Failed to encode file content: %s\n", opath);
		            }
                    }

                    free(file_content);
                } else {
                    printf("Memory allocation failed.\n");
                }
                
                fclose(file);
            } else {
                printf("Failed to open file: %s\n", opath);
            }
    	}
    	
    	rename(opath,npath);
    	
        struct stat st;
	
        memset(&st, 0, sizeof(st));

        st.st_ino = de->d_ino;
        st.st_mode = de->d_type << 12;

        res = (filler(buf, de->d_name, &st, 0));

        if (res != 0)
            break;
    }

    closedir(dp);

    return 0;
}

static int xmp_read(const char *path, char *buf, size_t size, off_t offset, struct fuse_file_info *fi) {
    char fpath[1000];

    if (strcmp(path, "/") == 0) {
        path = dirpath;
        sprintf(fpath, "%s", path);
    } else
        sprintf(fpath, "%s%s", dirpath, path);

    int res = 0;
    int fd = 0;

    (void)fi;

    fd = open(fpath, O_RDONLY);

    if (fd == -1)
        return -errno;

    res = pread(fd, buf, size, offset);

    if (res == -1)
        res = -errno;

    close(fd);

    return res;
}

static int xmp_mkdir(const char *path, mode_t mode) {
    char fpath[1000], npath[1000];
    sprintf(fpath, "%s%s", dirpath, path);
        
    // Check if the directory name starts with L, U, T, or H
    char f[1000], base[1000];
    strcpy(f,strrchr(path, '/') + 1);
    
    if(strlen(f) <= 4)
        strcpy(base,getBinaryRepresentation(f));
    else
        strcpy(base,toUpper(f));
    
    strcpy(npath,fpath);
    char *lastSlash = strrchr(npath, '/')+1;
    if (lastSlash != NULL) 
        *lastSlash = '\0';
    strcat(npath,base); 
    
    char *lastSlashOr = strrchr(path, '/')+1;
    if (lastSlashOr != NULL) 
        *lastSlashOr = '\0';
    strcat(path,base);
    
    int res = mkdir(npath, mode); 
    
    if (res == -1)
        return -errno;
        
    return 0;
}

static int xmp_rename(const char *from, const char *to) {

    char fpath_from[1000];
    char fpath_to[1000];
    char npath[1000];
    sprintf(fpath_from, "%s%s", dirpath, from);
    sprintf(fpath_to, "%s%s", dirpath, to);
    
    struct stat st;
    int rest = lstat(fpath_from, &st);
    if (rest == -1)
        return -errno;
    
    char f[1000], base[1000];
    strcpy(f,strrchr(to, '/') + 1);
    //printf("%s\n",f);
    
    if(strlen(f) <= 4)
        //printf("bin\n");
        strcpy(base,getBinaryRepresentation(f));
    else if(S_ISDIR(st.st_mode))
        //printf("dir\n");
        strcpy(base,toUpper(f));
    else
        //printf("file\n");
        strcpy(base,toLower(f));
        
    strcpy(npath,fpath_from);
    char *lastSlash = strrchr(npath, '/')+1;
    if (lastSlash != NULL) 
        *lastSlash = '\0';
    strcat(npath,base); 
    
    char *lastSlashOr = strrchr(to, '/')+1;
    if (lastSlashOr != NULL) 
        *lastSlashOr = '\0';
    strcat(to,base);
    
    //printf("%s\n",base);
 
    int res = rename(fpath_from, npath);
    
    if (res == -1)
        return -errno;

    return 0;
}

static bool verifyPassword(const char* enteredPassword) {
    const char* correctPassword = "0311";
    return strcmp(enteredPassword, correctPassword) == 0;
}

static int xmp_open(const char *path, struct fuse_file_info *fi) {
    char fpath[1000];
    sprintf(fpath, "%s%s", dirpath, path);
    //printf("%s\n",path);

    //Prompt for password
    char password[100];
    printf("Enter password: ");
    scanf("%s", password);

    if (!verifyPassword(password)) {
        printf("Incorrect password. Access denied.\n");
        return -EACCES; // Return appropriate error code for access denied
    }

    int fd = open(fpath, fi->flags);
    if (fd == -1)
        return -errno;

    fi->fh = fd;
    return 0;
}


static struct fuse_operations xmp_oper = {
    .getattr = xmp_getattr,
    .readdir = xmp_readdir,
    .read = xmp_read,
    .mkdir = xmp_mkdir,
    .rename = xmp_rename,
    .open = xmp_open, 
};

int main(int argc, char *argv[]) {
    umask(0);

    return fuse_main(argc, argv, &xmp_oper, NULL);
}
```
## Tes Output
Mounting Folder     
![soal3](https://i.ibb.co/Ycq0vtW/image.png)    

Password untuk membuka file     
![soal3](https://i.ibb.co/n0ZSYDD/image.png)