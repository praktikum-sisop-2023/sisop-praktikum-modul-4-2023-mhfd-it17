#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <dirent.h>
#include <sys/stat.h>
#include <sys/types.h>
#include <unistd.h>
#include <wait.h>
#include <fuse.h>
#include <time.h>
#include <paswdd.h>
#include <fcntl.h>
#include <errno.h>

#define FUSE_USE_VERSION 28
#define HOME "/home/alma/demo/"

//username dari UID yang menjalankan program.
void get_user(char *username) {
    //mendapatkan UID dan informasi dari pengguna yang menjalankan program
    uid_t uid = geteuid(); 
    struct passwd *paswd = getpaswduid(uid);

    //jika berhasil
    if (paswd != NULL) {
        //nama pengguna diambil dan disalin ke username
        snprintf(username, 255, "%s", paswd->paswd_name);
    } else {
        //jika tidak akan dicetak error
        fprintf(stderr, "Gagal mendapat username.\n");
        exit(1);
    }
}

int status;
//tanggal dan waktu
void getCurrentDateTime(char *dateTime) {
    time_t t = time(NULL);
    struct tm *tm = localtime(&t);
    strftime(datetime, 20, "%Y-%m-%d %H:%M:%S", tm);
}

void saveStatusKeFile(const char *status, const char *command, const char *deskripsi) {
    char datetime[50];
    char username[255];

    getCurrentDateTime(datetime);
    get_user(username);

    FILE *file_Log = fopen("/home/alma/demo/logmucatatsini.txt", "a");

    if (file_Log != NULL) {
        fprintf(file_Log, "[%s]::%s::%s::%s-%s\n", status, datetime, command, username, deskripsi);
        fclose(file_Log);
    } else {
        printf("Gagal membuka file log.\n");
        exit(1);
    }
}

//Download file dari drive yang ditentukan
void download_file(char *link_url, char *save_direktori){
    pid_t pid = fork(); 

    if (pid == 0) {
        execl("/usr/bin/wget", "wget", "--quiet", "--no-check-certificate", link_url, "-O", save_direktori, NULL);
        exit(0);
    } else if (pid > 0) {
        int status;
        waitpid(pid, &status, 0);

        if (WIFEXITED(status) && WEXITSTATUS(status) == 0) {
            saveStatusKeFile("SUCCESS", "DOWNLOAD", "Download completed successfully.");
        } else {
            saveStatusKeFile("FAILED", "DOWNLOAD", "Download failed.");
        }
    } else {
        fprintf(stderr, "Gagal pada proses fork.\n");
        exit(1);
    }
}

//unzip foldernya
void unzip_file(char *zip_file, char *unzip_direktori) {
    pid_t pid;
    int status;

    //diperiksa apa zip ada disistem
    if (access(zip_file, F_OK) == -1) {
        fprintf(stderr, "File zip tidak ditemukan.\n");
        exit(1);
    }

    pid = fork();
    if (pid == 0) { 
        //output ga ditampilin
        freopen("/dev/null", "w", stdout); 
        //nge unzip -> nimpa file yg ada saat ekstraksi
        execlp("unzip", "unzip", "-o", zip_file, "-d",  unzip_direktori, NULL);
        exit(1); 
    } else if (pid > 0) {
        waitpid(pid, &status, 0);
        if (WIFEXITED(status) && WEXITSTATUS(status) == 0) {
            saveStatusKeFile("SUCCESS", "UNZIP", "Unzip completed successfully.");
        } else {
            saveStatusKeFile("FAILED", "UNZIP", "Unzip failed.");
        }
    } else { 
        perror("fork");
        exit(1);
    }
}

static  const  char *pathDirektori = "/home/alma/demo/nanaxgerma/src_data";

//mencari file, mengelola file, dan simpan data
//mencari file yang dibutuhkan
static  int  custom_getattr(const char *path, struct stat *stbuf)
{
    int res;
    char path_files[1024];
    snprintf(path_files, sizeof(path_files), "%s%s", pathDirektori, path);
    res = lstat(path_files, stbuf);
    if (res == -1) 
        return -errno;
    return 0;
}

//membaca isi file dan mengembalikan data yang dibaca
static int custom_read(const char *path, char *buf, size_t size, off_t offset, struct fuse_file_info *fi)
{
    char path_files[1024];
    //ngecek path itu root atau tidak
    if(strcmp(path,"/") == 0)
    {
        path=pathDirektori;
        sprintf(path_files, sizeof(path_files), "%s",path);
    }
    else sprintf(path_files, sizeof(path_files), "%s%s",pathDirektori,path);

    int res = 0;
    int fd = 0 ;
    (void) fi;
    fd = open(path_files, O_RDONLY);
    if (fd == -1) 
        return -errno;
    res = pread(fd, buf, size, offset);

    if (res == -1) 
        res = -errno;
    close(fd);
    return res;
}

//membaca isi direktori
static int custom_readdir(const char *path, void *buf, fuse_fill_dir_t filler, off_t offset, struct fuse_file_info *fi)
{
    char path_files[1024];
    if(strcmp(path,"/") == 0)
    {
        path=pathDirektori;
        snprintf(path_files, sizeof(path_files), "%s", path);
    } else 
    {
        snprintf(path_files, sizeof(path_files), "%s%s", pathDirektori, path);
    }

    int res = 0;
    DIR *dp;
    struct dirent *de;
    (void) offset;
    (void) fi;

    dp = opendir(path_files);

    if (dp == NULL) 
        return -errno;
    struct stat stbuf;
    memset(&stbuf, 0, sizeof(stbuf));

    char filepath[1024];
    snprintf(filepath, sizeof(filepath), "%s/%s", path_files, de->d_name);

    if (lstat(filepath, &stbuf) == -1) {
        return -errno;
    }

    int res = 0;
    res = filler(buf, de->d_name, &stbuf, 0);

    if (res != 0) {
        break;
    }
    closedir(dp);
    return 0;
}

static int custom_create(const char *path, mode_t mode, struct fuse_file_info *fi)
{
    char path_files[1024];
    char text[100];
    if (strcmp(path, "/") == 0)
    {
        path = pathDirektori;
        snprintf(path_files, sizeof(path_files), "%s", path);
    }
    else
    {
        snprintf(path_files, sizeof(path_files), "%s%s", pathDirektori, path);
    }

    snprintf(text, sizeof(text), "Create file %s", path);

    // Mengecek kata kunci bypass dan restricted
    if (strstr(path, "restricted") != NULL)
    {
        // Jika path ada kata "restricted", log tidak berhasil
        saveStatusKeFile("FAILED", "CREATE", "Failed to create file");
        return -1;
    }
    else if (strstr(path, "bypass") != NULL && strstr(path, "restricted") != NULL)
    {
        // Jika path ada kata "bypass" dan "restricted", file dibuat
        int fd = creat(path_files, mode);
        if (fd == -1)
        {
            // Jika terjadi error, log tidak berhasil dan tampil error
            saveStatusKeFile("FAILED", "CREATE", "Failed to create file");
            return -errno;
        }
        else
        {
            // Jika berhasil, log sukses
            saveStatusKeFile("SUCCESS", "CREATE", text);
            close(fd);
            return 0;
        }
    }
    else
    {
        // Jika tidak ada kata "bypass" dan "restricted", file dibuat
        int fd = creat(path_files, mode);
        if (fd == -1)
        {
            // Jika terjadi error, log tidak berhasil dan tampil error
            saveStatusKeFile("FAILED", "CREATE", "Failed to create file");
            return -errno;
        }
        else
        {
            // Jika berhasil, log sukses
            saveStatusKeFile("SUCCESS", "CREATE", text);
            close(fd);
            return 0;
        }
    }
}

//membuat folder
static int custom_mkdir(const char *path, mode_t mode)
{
    char path_files[1024];
    char text[100];
    if (strcmp(path, "/") == 0)
    {
        path = pathDirektori;
        snprintf(path_files, sizeof(path_files), "%s", path);
    }
    else
    {
        snprintf(path_files, sizeof(path_files), "%s%s", pathDirektori, path);
    }

    snprintf(text, sizeof(text), "Create directory %s", path);
    
    if (strstr(path, "restricted") != NULL)
    {
        // Jika path ada kata "restricted", log tidak berhasil
        saveStatusKeFile("FAILED", "MKDIR", text);
        return -1;
    }
    // Mengecek kata kunci bypass
    else if (strstr(path, "bypass") != NULL)
    {
        // Jika path ada kata "bypass", direktori dibuat
        int res = mkdir(path_files, mode);
        if (res == -1)
        {
            // Jika terjadi error, log tidak berhasil dan tampil error
            saveStatusKeFile("FAILED", "MKDIR", "Creating directory failed");
            return -errno;
        }
        else
        {
            // Jika berhasil, log sukses
            saveStatusKeFile("SUCCESS", "MKDIR", text);
            return res;
        }
    }
    else
    {
        // Jika tidak ada kata "bypass" dan "restricted", direktori akan dibuat
        int res = mkdir(path_files, mode);
        if (res == -1)
        {
            // Jika terjadi error, log tidak berhasil dan tampil error
            saveStatusKeFile("FAILED", "MKDIR", "Creating directory failed");
            return -errno;
        }
        else
        {
            // Jika berhasil, log sukses
            saveStatusKeFile("SUCCESS", "MKDIR", text);
            return res;
        }
    }
}

static int custom_rename(const char *asal, const char *tujuan)
{
    char fpathAsal[1024];
    char fpathTujuan[1024];
    char text[1024];
    
    if (strcmp(asal, "/") == 0)
    {
        asal = pathDirektori;
        sprintf(fpathAsal, "%s", asal);
    }
    else
    {
        sprintf(fpathAsal, "%s%s", pathDirektori, asal);
    }

    if (strcmp(tujuan, "/") == 0)
    {
        tujuan = pathDirektori;
        sprintf(fpathTujuan, "%s", tujuan);
    }
    else
    {
        sprintf(fpathTujuan, "%s%s", pathDirektori, tujuan);
    }

    sprintf(text, "Rename from %s to %s" , asal, tujuan);

    if (strstr(asal, "restricted") != NULL)
    {
        // Jika path ada kata hanya "restricted", log tdak berhasil
        saveStatusKeFile("FAILED", "RENAME", "Failed to rename file");
        return -1;
    }
    else if (strstr(asal, "restricted") != NULL && strstr(asal, "bypass") != NULL)
    {
        int res = rename(fpathAsal, fpathTujuan);;
        if (res == -1)
        {
            // Jika terjadi error, log tidak berhasil dan tampil error
            saveStatusKeFile("FAILED", "RENAME", "Failed to rename file");
            return -errno;
        }
        else
        {
            // Jika berhasil, log sukses
            saveStatusKeFile("SUCCESS", "RENAME", text);
            return 0;
        }
    }
    else
    {
        // Jika tidak ada kata "restricted" dan "bypass", file direname
        int res = rename(fpathAsal, fpathTujuan);
        if (res == -1)
        {
            // Jika terjadi error, log tidak berhasil dan tampil error
            saveStatusKeFile("FAILED", "RENAME", "Failed to rename file");
            return -errno;
        }
        else
        {
            // Jika berhasil, log sukses
            saveStatusKeFile("SUCCESS", "RENAME", text);
            return 0;
        }
    }
}

//Menghapus file
static int custom_unlink(const char *path)
{
    char path_files[1024];
    char text[100];
    if (strcmp(path, "/") == 0)
    {
        path = pathDirektori;
        sprintf(path_files, "%s", path);
    }
    else
    {
        sprintf(path_files, "%s%s", pathDirektori, path);
    }

    sprintf(text, "Remove file %s", path);

    if (strstr(path, "restricted") != NULL)
    {
        // Jika path ada kata hanya "restricted", log tidak berhasil
        saveStatusKeFile("FAILED", "RMFILE", "Failed to remove file");
        return -1;
    }
    //Mengecek kata "restricted" dan "bypass" di path
    else if (strstr(path, "restricted") != NULL && strstr(path, "bypass") != NULL)
    {
        int res = unlink(path_files);
        if (res == -1)
        {
            // Jika terjadi error, log tidak berhasil dan tampil error
            saveStatusKeFile("FAILED", "RMFILE", "Failed to remove file");
            return -errno;
        }
        else
        {
            // Jika berhasil, log sukses
            saveStatusKeFile("SUCCESS", "RMFILE", text);
            return 0;
        }
    }
    else
    {
        // Jika tidak ada kata "restricted" dan "bypass", file dihapus
        int res = unlink(path_files);
        if (res == -1)
        {
            // Jika terjadi error, log tidak berhasil dan tampil error
            saveStatusKeFile("FAILED", "RMFILE", "Failed to remove file");
            return -errno;
        }
        else
        {
            // Jika berhasil, log sukses
            saveStatusKeFile("SUCCESS", "RMFILE", text);
            return 0;
        }
    }
}

static int custom_rmdir(const char *path)
{
    char path_files[1024];
    char text[100];
    if (strcmp(path, "/") == 0)
    {
        path = pathDirektori;
        sprintf(path_files, "%s", path);
    }
    else
    {
        sprintf(path_files, "%s%s", pathDirektori, path);
    }

    sprintf(text, "Remove directory %s", path);

    // Mengecek kata kunci restricted
    if (strstr(path, "restricted") != NULL)
    {
        // Jika path ada kata "restricted", log tidak berhasil
        saveStatusKeFile("FAIL", "REMOVE", text);
        return -1;
    }
    // Mengecek kata kunci bypass
    else if (strstr(path, "bypass") != NULL)
    {
        // Jika path ada kata "bypass", direktori dihapus
        int res = rmdir(path_files);
        if (res == -1)
        {
            // Jika terjadi error, log tidak berhasil dan tampil error
            saveStatusKeFile("FAIL", "REMOVE", text);
            return -errno;
        }
        else
        {
            // Jika berhasil, log sukses
            saveStatusKeFile("SUCCESS", "REMOVE", text);
            return 0;
        }
    }
    else
    {
        // Jika tidak ada kata "bypass", maka log tidak berhasil
        saveStatusKeFile("FAIL", "REMOVE", text);
        return -1;
    }
}

//Membuat fungsi operator fuse
static struct fuse_operations xmp_oper = {
    .getattr = custom_getattr,
    .readdir = custom_readdir, 
    .read = custom_read,
    .mkdir = custom_mkdir,
    .create = custom_create, 
    .rename = custom_rename, 
    .unlink = custom_unlink, 
    .rmdir = custom_rmdir,  
};

void init(){
    download_file(url, "./nanaxgerma.zip");
    char *url = "https://drive.google.com/uc?export=download&id=1iUZuYjgwcnaaEPzREPgfU6L8TYJ9633i";
   
    char* unzip_direktori = "/home/alma/demo";
    char* zip_file = "nanaxgerma.zip";
    unzip_file(zip_file, unzip_direktori);
}

int main(int argc, char *argv[]) {
    const char *status = "SUCCESS";
    const char *command = "SOME_COMMAND";
    const char *deskripsi = "desk";

    saveStatusKeFile(status, command, deskripsi);

    pid_t pid;
    pid = fork(); 
    //Parent process akan melakukan exit dengan status success (EXIT_SUCCESS), sedangkan child process 
    //akan melanjutkan untuk menjalankan fungsi init().
    if (pid<0){
        exit(EXIT_FAILURE);
    }
    if (pid > 0){
        exit(EXIT_SUCCESS);
    }

    init();
    umask(0);
    //untuk menjalankan FUSE dengan menggunakan operasi yang didefinisikan
    return fuse_main(argc, argv, &xmp_oper, NULL);
    return 0;
}
