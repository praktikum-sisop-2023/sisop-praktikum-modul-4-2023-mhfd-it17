#define FUSE_USE_VERSION 28
#include <fuse.h>
#include <stdio.h>
#include <string.h>
#include <unistd.h>
#include <fcntl.h>
#include <dirent.h>
#include <errno.h>
#include <sys/time.h>
#include <stdlib.h>
#include <stdint.h>
#include <ctype.h>
#include <stdbool.h> 

static const char *dirpath = "/home/dzaki/sisop/inifolderetc/sisop";

static const char base64_table[] = "ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz0123456789+/";

char* base64_encode(const char* input) {
    size_t input_length = strlen(input);
    size_t output_length = 4 * ((input_length + 2) / 3); // Calculate the output length

    char* encoded_data = (char*)malloc(output_length + 1); // Allocate memory for the encoded data

    if (encoded_data == NULL) {
        printf("Memory allocation failed.\n");
        return NULL;
    }

    size_t i, j;
    for (i = 0, j = 0; i < input_length;) {
        uint32_t octet_a = i < input_length ? (unsigned char)input[i++] : 0;
        uint32_t octet_b = i < input_length ? (unsigned char)input[i++] : 0;
        uint32_t octet_c = i < input_length ? (unsigned char)input[i++] : 0;

        uint32_t triple = (octet_a << 16) + (octet_b << 8) + octet_c;

        encoded_data[j++] = base64_table[(triple >> 18) & 0x3F];
        encoded_data[j++] = base64_table[(triple >> 12) & 0x3F];
        encoded_data[j++] = base64_table[(triple >> 6) & 0x3F];
        encoded_data[j++] = base64_table[triple & 0x3F];
    }

    // Pad the encoded data if necessary
    size_t padding = input_length % 3;
    for (i = 0; i < padding; i++) {
        encoded_data[output_length - 1 - i] = '=';
    }

    encoded_data[output_length] = '\0'; // Add null terminator

    return encoded_data;
}

char* toUpper(char* str) {
    int i = 0;
    while (str[i]) {
        str[i] = toupper(str[i]);
        i++;
    }
    return str;
}

char* toLower(char* str) {
    int i = 0;
    while (str[i]) {
        str[i] = tolower(str[i]);
        i++;
    }
    return str;
}

char* getBinaryRepresentation(const char* str) {
    int length = strlen(str);
    char* binaryStr = (char*)malloc((8 * length + 1) * sizeof(char)); // Allocate memory for binary string

    if (binaryStr == NULL) {
        printf("Memory allocation failed.\n");
        return NULL;
    }

    int index = 0;
    for (int i = 0; i < length; i++) {
        unsigned char ch = (unsigned char)str[i]; // Cast to unsigned char to ensure consistent behavior
        for (int j = 7; j >= 0; j--) {
            binaryStr[index++] = ((ch >> j) & 1) + '0'; // Convert bit to character '0' or '1'
        }
        binaryStr[index++] = ' ';
    }
    binaryStr[index - 1] = '\0'; // Replace the last space with null character

    return binaryStr;
}

static int xmp_getattr(const char *path, struct stat *stbuf) {
    int res;
    char fpath[1000];

    sprintf(fpath, "%s%s", dirpath, path);

    res = lstat(fpath, stbuf);

    if (res == -1)
        return -errno;

    return 0;
}

static int xmp_readdir(const char *path, void *buf, fuse_fill_dir_t filler, off_t offset, struct fuse_file_info *fi) {
    char fpath[1000];

    if (strcmp(path, "/") == 0) {
        path = dirpath;
        sprintf(fpath, "%s", path);
    } else
        sprintf(fpath, "%s%s", dirpath, path);

    int res = 0;

    DIR *dp;
    struct dirent *de;
    (void)offset;
    (void)fi;

    dp = opendir(fpath);
    //printf("%s\n",fpath);
    if (dp == NULL)
        return -errno;

    while ((de = readdir(dp)) != NULL) {
    
        char f[1000], base[1000], opath[1000], npath[1000];
        strcpy(f,de->d_name);
        
        if(strlen(f) <= 4 )
        {
            strcpy(base,getBinaryRepresentation(f));
            //printf("bin - %s -> %s\n", f, base);
        }
        else if(de->d_type == DT_DIR && (f[0] != 0 && f[0] != 1))
        {
            strcpy(base,toUpper(f));
            //printf("dir - %s -> %s\n", f, base);
        }
        else if(de->d_type == DT_REG && (f[0] != 0 && f[0] != 1))
        {
            strcpy(base,toLower(f));
            //printf("file - %s -> %s\n", f, base);
        }
        else
            strcpy(base,f);
        
        strcpy(opath,fpath);
    	strcpy(npath,fpath);
    	strcat(opath,"/");
    	strcat(npath,"/");
    	strcat(opath,de->d_name);
    	strcat(npath,base);
    	
        
    	if ((de->d_type == DT_REG) && (
    	     f[0] == 'L' || f[0] == 'U' || f[0] == 'T' || f[0] == 'H' ||
             f[0] == 'l' || f[0] == 'u' || f[0] == 't' || f[0] == 'h'))
    	{
    	     // Open the file for reading
            FILE* file = fopen(opath, "rb");
            if (file != NULL) {
                // Determine the file size
                fseek(file, 0, SEEK_END);
                long file_size = ftell(file);
                fseek(file, 0, SEEK_SET);

                // Allocate memory for file content
                char* file_content = (char*)malloc(file_size);
                if (file_content != NULL) {
                    // Read the file content
                    fread(file_content, 1, file_size, file);
                    
                    if(strchr(file_content, '!') == NULL){
		             // Encode the file content with base64
		            char* encoded_content = base64_encode(file_content);
		            strcat(encoded_content,"\n!");
		            if (encoded_content != NULL) {
		                // Create a new file with the encoded content
		                FILE* encoded_file = fopen(npath, "wb");
		                if (encoded_file != NULL) {
		                    // Write the encoded content to the file
		                    fwrite(encoded_content, 1, strlen(encoded_content), encoded_file);
		                    fclose(encoded_file);
		                } else {
		                    printf("Failed to create the encoded file: %s\n", npath);
		                }
		                
		                free(encoded_content);
		            } else {
		                printf("Failed to encode file content: %s\n", opath);
		            }
                    }

                    free(file_content);
                } else {
                    printf("Memory allocation failed.\n");
                }
                
                fclose(file);
            } else {
                printf("Failed to open file: %s\n", opath);
            }
    	}
    	
    	rename(opath,npath);
    	
        struct stat st;
	
        memset(&st, 0, sizeof(st));

        st.st_ino = de->d_ino;
        st.st_mode = de->d_type << 12;

        res = (filler(buf, de->d_name, &st, 0));

        if (res != 0)
            break;
    }

    closedir(dp);

    return 0;
}

static int xmp_read(const char *path, char *buf, size_t size, off_t offset, struct fuse_file_info *fi) {
    char fpath[1000];

    if (strcmp(path, "/") == 0) {
        path = dirpath;
        sprintf(fpath, "%s", path);
    } else
        sprintf(fpath, "%s%s", dirpath, path);

    int res = 0;
    int fd = 0;

    (void)fi;

    fd = open(fpath, O_RDONLY);

    if (fd == -1)
        return -errno;

    res = pread(fd, buf, size, offset);

    if (res == -1)
        res = -errno;

    close(fd);

    return res;
}

static int xmp_mkdir(const char *path, mode_t mode) {
    char fpath[1000], npath[1000];
    sprintf(fpath, "%s%s", dirpath, path);
        
    // Check if the directory name starts with L, U, T, or H
    char f[1000], base[1000];
    strcpy(f,strrchr(path, '/') + 1);
    
    if(strlen(f) <= 4)
        strcpy(base,getBinaryRepresentation(f));
    else
        strcpy(base,toUpper(f));
    
    strcpy(npath,fpath);
    char *lastSlash = strrchr(npath, '/')+1;
    if (lastSlash != NULL) 
        *lastSlash = '\0';
    strcat(npath,base); 
    
    char *lastSlashOr = strrchr(path, '/')+1;
    if (lastSlashOr != NULL) 
        *lastSlashOr = '\0';
    strcat(path,base);
    
    int res = mkdir(npath, mode); 
    
    if (res == -1)
        return -errno;
        
    return 0;
}

static int xmp_rename(const char *from, const char *to) {

    char fpath_from[1000];
    char fpath_to[1000];
    char npath[1000];
    sprintf(fpath_from, "%s%s", dirpath, from);
    sprintf(fpath_to, "%s%s", dirpath, to);
    
    struct stat st;
    int rest = lstat(fpath_from, &st);
    if (rest == -1)
        return -errno;
    
    char f[1000], base[1000];
    strcpy(f,strrchr(to, '/') + 1);
    //printf("%s\n",f);
    
    if(strlen(f) <= 4)
        //printf("bin\n");
        strcpy(base,getBinaryRepresentation(f));
    else if(S_ISDIR(st.st_mode))
        //printf("dir\n");
        strcpy(base,toUpper(f));
    else
        //printf("file\n");
        strcpy(base,toLower(f));
        
    strcpy(npath,fpath_from);
    char *lastSlash = strrchr(npath, '/')+1;
    if (lastSlash != NULL) 
        *lastSlash = '\0';
    strcat(npath,base); 
    
    char *lastSlashOr = strrchr(to, '/')+1;
    if (lastSlashOr != NULL) 
        *lastSlashOr = '\0';
    strcat(to,base);
    
    //printf("%s\n",base);
 
    int res = rename(fpath_from, npath);
    
    if (res == -1)
        return -errno;

    return 0;
}

static bool verifyPassword(const char* enteredPassword) {
    const char* correctPassword = "0311";
    return strcmp(enteredPassword, correctPassword) == 0;
}

static int xmp_open(const char *path, struct fuse_file_info *fi) {
    char fpath[1000];
    sprintf(fpath, "%s%s", dirpath, path);
    //printf("%s\n",path);

    //Prompt for password
    char password[100];
    printf("Enter password: ");
    scanf("%s", password);

    if (!verifyPassword(password)) {
        printf("Incorrect password. Access denied.\n");
        return -EACCES; // Return appropriate error code for access denied
    }

    int fd = open(fpath, fi->flags);
    if (fd == -1)
        return -errno;

    fi->fh = fd;
    return 0;
}


static struct fuse_operations xmp_oper = {
    .getattr = xmp_getattr,
    .readdir = xmp_readdir,
    .read = xmp_read,
    .mkdir = xmp_mkdir,
    .rename = xmp_rename,
    .open = xmp_open, 
};

int main(int argc, char *argv[]) {
    umask(0);

    return fuse_main(argc, argv, &xmp_oper, NULL);
}