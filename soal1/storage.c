#include <unistd.h>
#include <fcntl.h>
#include <dirent.h>
#include <errno.h>
#include <sys/time.h>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>

#define MAX_PLAYERS 20000
#define MAX_LINE_LENGTH 20000

typedef struct {
  char name[100];
  int age;
  int potential;
  char club[50];
  char nationality[100];
  char photo[100];
} Player;

int main() {

   if (system("kaggle datasets download -d bryanb/fifa-player-stats-database") != 0)
    {
       printf("\e[31m\nDataset gagal didownload.\e[0m\n");
       return 1;
    }

   if (system("unzip fifa-player-stats-database.zip") != 0)
    {
       printf("\e[31m\nDataset gagal diekstrak.\e[0m\n");
       return 1;
    }
       printf("\e[32m\nDataset berhasil didownload dan diekstrak.\e[0m\n");


    FILE *file_ptr;
    char line[MAX_LINE_LENGTH];
    Player players_array[MAX_PLAYERS];
    int num_players = 0;

    file_ptr = fopen("FIFA23_official_data.csv", "r");
    
    if (file_ptr == NULL) {
        printf("\e[31m\nGagal membuka file.\e[0m\n");
        return 1;
    }

    fgets(line, sizeof(line), file_ptr); 

    while (fgets(line, sizeof(line), file_ptr)) {
        char *token = strtok(line, ",");
        int field_count = 0;
        Player player_info;

        while (token != NULL && field_count <= 8) {
            switch (field_count) {
                case 1:
                    strncpy(player_info.name, token, sizeof(player_info.name));
                    break;
                case 2:
                    player_info.age = atoi(token);
                    break;
                case 7:
                    player_info.potential = atoi(token);
                    break;
                case 8:
                    strncpy(player_info.club, token, sizeof(player_info.club));
                    break;
                case 4:
                    strncpy(player_info.nationality, token, sizeof(player_info.nationality));
                    break;
                case 3:
                    strncpy(player_info.photo, token, sizeof(player_info.photo));
                    break;
            }

            token = strtok(NULL, ",");
            field_count++;
        }

        if (field_count > 0 && player_info.age < 25 && player_info.potential > 85 && strcmp(player_info.club, "Manchester City") != 0) {
            if (num_players >= MAX_PLAYERS) {
                printf("\e[31m\nJumlah player sudah mencapai maksimal.\e[0m\n");
                break;
            }
            players_array[num_players++] = player_info;
        }
    }

    fclose(file_ptr);

    for (int i = 0; i < num_players; i++) {
        printf("\e[36mName:\e[0m %s\n", players_array[i].name);
        printf("\e[36mAge:\e[0m %d\n", players_array[i].age);
        printf("\e[36mPotential:\e[0m %d\n", players_array[i].potential);
        printf("\e[36mClub:\e[0m %s\n", players_array[i].club);
        printf("\e[36mNationality:\e[0m %s\n", players_array[i].nationality);
        printf("\e[36mPhoto:\e[0m %s\n", players_array[i].photo);
        printf("\e[33m--------------------\e[33m\n");
    }

    return 0;
}
